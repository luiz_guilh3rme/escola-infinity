<?php 

/*
Template Name: Obrigado Home
*/

get_header(); 

?>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/css/fresco/fresco.css" />

<style type="text/css">
#map {
	height: 767px;
	width: 100%;
	margin-top: 0;
}
.form-home{
	top:0;
}
.sobre{
	border-bottom: 2px solid #f64a00;
}
@media (max-width: 480px) {



  .sobre{

   border-bottom: 0;

}

}

.cleared:after {
   content:  '';
   display: block;
   clear:  both;
}

.container .col-md-6.right {
   height: 2594px;
}
</style>


<div class="carrosel">
   <div>
    <div class="banner-interna banner-sobre laranja">
       <div class="container">
        <div class="texto-banner">
         <h1 style="margin-top:150px!important">Obrigado pelo <span>Contato!</span></h1>
         <p>Em breve retornaremos com mais informações!</p>
         <a href="https://www.escolainfinity.com.br" class="#"><span>Voltar para Home</span></a>
      </div>
   </div>

</div>

</div>

</div>
<img src="<?php echo get_template_directory_uri(); ?>/images/bush-laranja2.png" class="bush-banner">
<?php include_once ('includes/php/menu.php') ?>


<div class="container cleared">
   <div class="col-md-6 conteudo">
    <h2 style="letter-spacing: 0;">conheça o espaço físico <span>
    da escola INFINITY</span></h2>
    <ul class="galeria cleared">
      <li>      <a href='<?php echo get_template_directory_uri(); ?>/images/full1.jpg'
         class='fresco'
         data-fresco-group='Grupo1'>
         <img src='<?php echo get_template_directory_uri(); ?>/images/thumb1.jpg' alt=''/>
      </a>

   </li>
   <li>
      <a href='<?php echo get_template_directory_uri(); ?>/images/full2.jpg'
         class='fresco'
         data-fresco-group='Grupo1'>
         <img src='<?php echo get_template_directory_uri(); ?>/images/thumb2.jpg' alt=''/>
      </a>
   </li>
   <li>      <a href='<?php echo get_template_directory_uri(); ?>/images/full3.jpg'
      class='fresco'
      data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb3.jpg' alt=''/>
   </a>

</li>
<li>
   <a href='<?php echo get_template_directory_uri(); ?>/images/full4.jpg'
      class='fresco'
      data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb4.jpg' alt=''/>
   </a>
</li>
<li>      <a href='<?php echo get_template_directory_uri(); ?>/images/full5.jpg'
   class='fresco'
   data-fresco-group="Grupo1">
   <img src='<?php echo get_template_directory_uri(); ?>/images/thumb5.jpg' alt=''/>
</a>

</li>
<li>
   <a href='<?php echo get_template_directory_uri(); ?>/images/full6.jpg'
      class='fresco'
      data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb6.jpg' alt=''/>
   </a>
</li>
<li>      <a href='<?php echo get_template_directory_uri(); ?>/images/full7.jpg'
   class='fresco'
   data-fresco-group="Grupo1">
   <img src='<?php echo get_template_directory_uri(); ?>/images/thumb7.jpg' alt=''/>
</a>

</li>
<li>
   <a href='<?php echo get_template_directory_uri(); ?>/images/full8.jpg'
      class='fresco'
      data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb8.jpg' alt=''/>
   </a>
</li>




<li>      <a href='<?php echo get_template_directory_uri(); ?>/images/full9.jpg'
   class='fresco'
   data-fresco-group="Grupo1">
   <img src='<?php echo get_template_directory_uri(); ?>/images/thumb9.jpg' alt=''/>

</a>

</li>
<li>
   <a href='<?php echo get_template_directory_uri(); ?>/images/full10.jpg'
      class='fresco'
      data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb10.jpg' alt=''/>

   </a>
</li>
<li>      <a href='<?php echo get_template_directory_uri(); ?>/images/full11.jpg'
   class='fresco'
   data-fresco-group="Grupo1">
   <img src='<?php echo get_template_directory_uri(); ?>/images/thumb11.jpg' alt=''/>

</a>

</li>
<li>
   <a href='<?php echo get_template_directory_uri(); ?>/images/full12.jpg'
      class='fresco'
      data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb12.jpg' alt=''/>

   </a>
</li>
<li>      
   <a href='<?php echo get_template_directory_uri(); ?>/images/full13.jpg' class='fresco' data-fresco-group="Grupo1">
      <img src='<?php echo get_template_directory_uri(); ?>/images/thumb13.jpg' alt=''/>
   </a>
</li>
</ul>
<p class="deslize"><< Deslize para ver mais  >></p>
</div>
<div class="col-md-6 right">
  <?php include_once ('includes/php/form-sidebar.php') ?>
  <img src="<?php echo get_template_directory_uri(); ?>/images/auxiliar-sobre.jpg" class="imagem-auxiliar">
</div>
</div>
<img src="<?php echo get_template_directory_uri(); ?>/images/bush-laranja.png" class="bush-laranja">


<?php 


get_footer(); 

?>


